/* Generated automatically. */
static const char configuration_arguments[] = "../gcc/configure --target=arm-linux-gnueabihf --prefix=/home/Zephyc/gcc-build/../gcc-arm --disable-decimal-float --disable-libffi --disable-libgomp --disable-libmudflap --disable-libquadmath --disable-libstdcxx-pch --disable-nls --disable-shared --disable-docs --enable-default-ssp --enable-languages=c,c++ --with-pkgversion='Apocalypse GCC' --with-newlib --with-gnu-as --with-gnu-ld --with-sysroot";
static const char thread_model[] = "posix";

static const struct {
  const char *name, *value;
} configure_default_options[] = { { "cpu", "arm10tdmi" }, { "tls", "gnu" } };
